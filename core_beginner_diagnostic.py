'''
Diagnostic exam to test basic knowledge of Python.

1. There are six procedures to write.

2. Read the docstring thoroughly and then complete
you code in the section provided.

3. DO NOT CHANGE THE FUNCTION NAMES..
The names are set to allow automated checking of your
exam.  If the names are changed then your exam cannot be
graded.

4. PROVIDE COMMENTS...
Uncommented code is not useful code, even if it passes
tests.

5. For each procedure write a function that tests the
procedure and prints output for at least three different inputs.

The skeleton for the testing functions has already been
written for you.


Author: Michael Street
'''
from __future__ import division

#---------------------------------
# 1. Evaluating a polynomial
#---------------------------------

def evalPolynomial(x, a_n):
    '''
    Write a procedure to evaluate polynomials given
    the evaluation point and a list of coefficients
    ranked highest to lowest.

    Keyword Arguments:
    x - value at which to evaluate the polynomial
    a_n - list of coefficients ordered highest to lowest

    Returns:
    y - value of function at x

    >>> evalPolynomial(1, [4, 3, 1])
    8
    >>> evalPolynomial(2, [3, 1, 1])
    15
    '''

    # YOUR CODE GOES HERE

    pass

# Testing function 
def test_evalPolynomial():

    # YOUR CODE HERE
    
    pass

#---------------------------------
# 2. Evaluating simple quadratic roots
#---------------------------------
def simpleQuadraticEquation(a, b, c):
    '''
    Write a procedure to solve the quadratic equation
    that does not allow complex roots.

    Given a quadratic equation, find x such that:

    ax**2 + bx + c = 0

    where:
        x1 = (-b + math.sqrt(b**2 - 4ac))/(2*a))
        x2 = (-b - math.sqrt(b**2 - 4ac)/(2*a))

    The roots are complex if the determinant is less than
    zero:

    determinant = b**2 - 4ac

    Keyword Arguments:
    a - coefficient of x squared term
    b - coefficeint of x term
    c - constant coefficient

    
    Errors:
    - Raises AssertionError if a is zero
    - Raises AssertionError if the roots are complex

    Returns:
    (x1, x2) - tuple of with two roots of the polynomial

    '''

    # YOUR CODE GOES HERE
    pass

# Testing function 
def test_simpleQuadraticEquation():

    # YOUR CODE HERE
    pass

#---------------------------------
# 3. Evaluating quadratic roots w/ complex numbers
#---------------------------------
def quadraticEquation(a, b, c):
    '''
    Write a procedure to solve the quadratic equation
    that allows complex roots.
    
    Given a quadratic equation, find x such that:

    ax**2 + bx + c = 0

    where:
        x1 = (-b + math.sqrt(b**2 - 4ac))/(2*a))
        x2 = (-b - math.sqrt(b**2 - 4ac)/(2*a))

    The roots are complex if the determinant is less than
    zero:

    determinant = b**2 - 4ac

    A complex root has both a real and imaginary component.

    Keyword Arguments:
    a - coefficient of squared term
    b - coefficeint of x term
    c - constant coefficient

    
    Errors:
    - Raises AssertionError if a is zero
    
    Returns:
    ((x1_real, x1_img), (x2_real, x2_img) - two roots of the polynomial
    '''

    # YOUR CODE GOES HERE
    pass

# Testing function 
def test_quadraticEquation():

    # YOUR CODE HERE
    pass
#---------------------------------
# 4. Locating a word in a sentence
#---------------------------------
def wordCount(sentence, word):
    '''
    Write a procedure to receive a string of words and return the
    list of word locations.

    Keyword Arguments:
    sentence - string of words separated only by spaces
    word - test string to find in sentence    

    Assumes:
    - words separated only by spaces
    - no punctuation or capitalization

    Returns:
    False - if word does not appear
    wordIndexes - List of the word locations
    
    >>> wordCount("she flies she jumps she sells", "she")
    [0, 2, 4]

    '''

    # INSERT YOUR CODE HERE

    pass

# Testing function 
def test_wordCount():

    # YOUR CODE HERE
    pass
#---------------------------------
# 5. Locating ALL words in a sentence
#---------------------------------
def hashedWordCount(sentence):
    '''
    Write a procedure to create a hashed sentence search.
    Hashed search can be done with a dictionary.

    >>> sentence = "she flies she jumps she sells"
    >>> wordDict = hashedWordCount(sentence)
    >>> wordDict["she"]
    [0, 2, 4]
    >>> wordDict["flies"]
    [1]

    Keyword Arguments:
    sentence - string of words separated only by spaces   

    Assumes:
    - words separated only by spaces
    - no punctuation or capitalization

    Returns:
    wordDictionary - dict of words with corresponding locations

    
    '''

    # YOUR CODE GOES HERE
    pass
# Testing function 
def test_hashedWordCount():

    # YOUR CODE HERE
    pass
#---------------------------------
# 6. Returning the proper change
#---------------------------------
def calculateChange(price, tendered):
    '''
    Write a procedure that gives integer change for
    items with integer prices.  Only return change
    of 20, 10, 5 or 1 dollar bills.

    Assumes:
    - Infinite supply of 20, 10, 5 and 1 dollar bills
    - price and tendered >= 0
    - price and tendered are integer values
    - no change given if no money tendered

    Errors:
    - Raises ValueError if price or tendered < 0
    - Raises ValueError if price or tendered aren't integer

    Keyword Arguments:
    price - price of goods sold
    tendered - amount of dollars given to cashier

    Returns:
    change - List of denominations and their values
    '''

    
    #YOUR CODE GOES HERE

    pass

# Testing function 
def test_calculateChange():

    # YOUR CODE HERE
    pass

#---------------------------------------
# Main Function
#---------------------------------------
def main():
    # This main function simply runs all of the above
    # test functions.
    test_evalPolynomial()
    test_simpleQuadraticEquation()
    test_quadraticEquation()
    test_wordCount()
    test_hashedWordCount()
    test_calculateChange()
    

if __name__ == "__main__":
    main()
