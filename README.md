#README

This is a basic diagnostic exam for students studying Python.

These problems should exercise your ability to break down small
problems.  Python has a number of helpful methods to make a 
good number of these problems relatively simple.

Be sure to plan out your thought process before writing code
for any of the methods!

# Checking Your Answers

A testing module has been included into this repository
to let you test your ability to write code that meets a given
specification.

First, simply write your answers into the file named

```python
core_beginner_diagnostic.py
```

Then from the terminal

```cmd
python core_tests.py
```

You're all set!  

Enjoy!